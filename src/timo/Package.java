/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.util.Arrays;

/**
 *
 * @author am
 */
abstract class Package {
    protected Object object;
    protected int packageClass;
    protected String[] addresses = new String[4];
    
    protected int getPackageClass() {
        return packageClass;
    }
    
    protected String getPackageAddresses() {
        return Arrays.toString(addresses);
    }
    
    @Override
    public String toString() {
        return (packageClass + ". luokan paketti");
    }
}

class FirstClassPackage extends Package {
    public FirstClassPackage(Object o, String flat, String flon, String tlat, String tlon) {
        object = o;
        packageClass = 1;
        addresses[0] = flat;
        addresses[1] = flon;
        addresses[2] = tlat;
        addresses[3] = tlon;
    }
}

class SecondClassPackage extends Package {
    public SecondClassPackage(Object o, String flat, String flon, String tlat, String tlon) {
        object = o;
        packageClass = 2;
        addresses[0] = flat;
        addresses[1] = flon;
        addresses[2] = tlat;
        addresses[3] = tlon;
    }
}

class ThirdClassPackage extends Package {
    public ThirdClassPackage(Object o, String flat, String flon, String tlat, String tlon) {
        object = o;
        packageClass = 3;
        addresses[0] = flat;
        addresses[1] = flon;
        addresses[2] = tlat;
        addresses[3] = tlon;
    }
}