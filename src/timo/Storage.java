/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.util.ArrayList;

/**
 *
 * @author am
 */
public class Storage {
    private ArrayList<Package> packagelist = new ArrayList();

    static private Storage s = null;

    static public Storage getInstance() {
        if (s == null)
            s = new Storage();
        return s;
    }
    
    public void addPackage(Package p) {
        packagelist.add(p);
    }
    
    public void removePackage(Package p) {
        packagelist.remove(p);
    }
    
    public Package getPackage(int i) {
        return packagelist.get(i);
    }
    
    public int getSize() {
        return packagelist.size();
    }
}
