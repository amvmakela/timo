/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.util.ArrayList;

/**
 *
 * @author am
 */
public class SmartPosts {
    private ArrayList<SmartPost> splist = new ArrayList();
    static private SmartPosts sp = null;
    
    static public SmartPosts getInstance() {
        if (sp == null)
            sp = new SmartPosts();
        return sp;
    }
    
    public void add(SmartPost p) {
        splist.add(p);
    }
    
    public int getListSize() {
        return splist.size();
    }
    
    public SmartPost getSmartPost(int i) {
        return splist.get(i);
    }
}
