/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author am
 */
public class DataBuilder {
    
    private Document doc;
    private ArrayList<SmartPost> splist = new ArrayList();
    
    static private DataBuilder db = null;
    
    private DataBuilder() {
        try {
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String content = "";
            String line;
            
            while((line = br.readLine()) != null) {
                content += line + "\n";
            }
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    static public DataBuilder getInstance() {
        if (db == null)
            db = new DataBuilder();
        return db;
    }
    
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("place");
        SmartPosts sp = SmartPosts.getInstance();
        
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String code = getTextValue("code", e);
            String city = getTextValue("city", e);
            String address = getTextValue("address", e);
            String availability =  getTextValue("availability", e);
            String postoffice = getTextValue("postoffice", e);
            String lat = getTextValue("lat", e);
            String lng = getTextValue("lng", e);
            
            sp.add(new SmartPost(code, city, address, availability ,postoffice, lat, lng));
            
        }
    }
    
    private String getTextValue(String tag, Element e) {
        
        NodeList nl = e.getElementsByTagName(tag);
        Element el = (Element)nl.item(0);
        String textVal = el.getFirstChild().getNodeValue();
        return textVal;
    }
}
