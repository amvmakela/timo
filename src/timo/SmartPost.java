/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author am
 */
public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    private String lat;
    private String lng;
    
    
    public SmartPost(String co, String ci, String ad, String av, String po, String la, String ln) {
        code = co;
        city = ci;
        address = ad;
        availability = av;
        postoffice = po;
        lat = la;
        lng = ln;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getLat() {
        return lat;
    }
    
    public String getLng() {
        return lng;
    }
    
    
    public String getAddress() {
        String add = address + ", " + code + " " + city; 
        return add;
    }
    
    
    public String getInfo() {
        String i = postoffice + ": " + availability;
        return i;
    }
    
    @Override
    public String toString() {
        return (city + ": " +  address + " " + code + " " + postoffice);
    }
}
