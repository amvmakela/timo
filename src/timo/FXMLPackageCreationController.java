/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

/**
 * FXML Controller class
 *
 * @author am
 */
public class FXMLPackageCreationController implements Initializable {
    @FXML
    private Font x1;
    @FXML
    private ComboBox<Object> itemsCombo;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemWeightField;
    @FXML
    private CheckBox fragileTick;
    @FXML
    private Button createButton;
    @FXML
    private Button infoButton;
    @FXML
    private ComboBox<String> fromCityCombo;
    @FXML
    private ComboBox<SmartPost> fromPostCombo;
    @FXML
    private ComboBox<String> toCityCombo;
    @FXML
    private ComboBox<SmartPost> toPostCombo;
    @FXML
    private ComboBox<Integer> classCombo;
    @FXML
    private Label statusLabel;
    @FXML
    private Label infoLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /* Adding the possible class choices for the package */
        classCombo.getItems().addAll(1, 2, 3);
        
        SmartPosts sp = SmartPosts.getInstance();
        int size = sp.getListSize();
        
        /* Filling the comboboxes with cities */
        for (int i = 0; i < size; i++) {
            String c = sp.getSmartPost(i).getCity();
            if (fromCityCombo.getItems().contains(c) != true) {
                fromCityCombo.getItems().add(c);
                toCityCombo.getItems().add(c);
            }
        }
        
        /* Filling the combobox with predetermined items */
        itemsCombo.getItems().add(new Spoon("Lusikka", 10, 1));
        itemsCombo.getItems().add(new Flowerpot("Kukkaruukku", 20, 2));
        itemsCombo.getItems().add(new Bicycle("Polkupyörä", 5000, 50, "Punainen"));
        itemsCombo.getItems().add(new Rock("Kivi", 800, 100, 5));
        
    }    

    @FXML
    private void createPackageAction(ActionEvent event) {
        checkInputs();
        if (checkInputs() == false) {
            statusLabel.setText("Tarkasta tiedot!");
        } else {
            if (itemsCombo.getValue() == null) {    /* If no item has been picked from the combobox */
                
                if (checkInputFields() == false) {
                    statusLabel.setText("Tarkasta tiedot!");
                } else {
                    int c = classCombo.getValue();

                    String name = itemNameField.getText();

                    int size = Integer.parseInt(itemSizeField.getText());
                    int weight = Integer.parseInt(itemWeightField.getText());

                    Object o = new Object(name, size, weight);
                    String flat = fromPostCombo.getValue().getLat();
                    String flng = fromPostCombo.getValue().getLng();
                    String tlat = toPostCombo.getValue().getLat();
                    String tlng = toPostCombo.getValue().getLng();

                    createPackage(o, c, flat, flng, tlat, tlng);
                }


            } else {    /* If item from the combobox has been picked */

                int c = classCombo.getValue();
                Object o =  itemsCombo.getValue();
                String flat = fromPostCombo.getValue().getLat();
                String flng = fromPostCombo.getValue().getLng();
                String tlat = toPostCombo.getValue().getLat();
                String tlng = toPostCombo.getValue().getLng();

                createPackage(o, c, flat, flng, tlat, tlng);
            }
        }
    }
    
    /* Creates the package and adds it to the storage */
    private void createPackage(Object o, int c, String flat, String flng, String tlat, String tlng) {
        
        Package p = null;
        switch (c) {
                case 1:
                    if (o.getWeight() < 10) {
                        p = new FirstClassPackage(o, flat, flng, tlat, tlng);
                    } else {
                        statusLabel.setText("Tarkasta luokkien tiedot!");
                    }
                break;
                case 2:
                    if (o.getSize() < 400) {
                        p = new SecondClassPackage(o, flat, flng, tlat, tlng);
                    } else {
                        statusLabel.setText("Tarkasta luokkien tiedot!");
                    }
                break;
                case 3:
                    p = new ThirdClassPackage(o, flat, flng, tlat, tlng);
                    break;
            }
        if (p != null) {
            Storage.getInstance().addPackage(p);
            clearSelections();
            statusLabel.setText("Paketti luotu!");
        } 
    }


    @FXML
    private void displayInfo(ActionEvent event) {
        infoLabel.setText("1. Nopein, max paino 10\n2. Max koko 400\n3. Hitain");
    }
    
    @FXML
    private void getFromSmartPosts(MouseEvent event) {
        fromPostCombo.getItems().clear();
        
        SmartPosts sp = SmartPosts.getInstance();
        int size = sp.getListSize();
        
        String city = fromCityCombo.getValue();
        
        for (int i = 0; i < size; i++) {
            if (sp.getSmartPost(i).getCity().equals(city)){
                fromPostCombo.getItems().add(sp.getSmartPost(i));
            }
        }
    }

    @FXML
    private void getToSmartPosts(MouseEvent event) {
        toPostCombo.getItems().clear();
        
        SmartPosts sp = SmartPosts.getInstance();
        int size = sp.getListSize();
        
        String city = toCityCombo.getValue();
        for (int i = 0; i < size; i++) {
            if (sp.getSmartPost(i).getCity().equals(city)){
                toPostCombo.getItems().add(sp.getSmartPost(i));
            }
        }
    }
    
    /* Clears all the selections */
    private void clearSelections() {
        itemsCombo.getSelectionModel().clearSelection();
        classCombo.getSelectionModel().clearSelection();
        itemNameField.clear();
        itemSizeField.clear();
        itemWeightField.clear();
        fragileTick.setSelected(false);
        fromCityCombo.getSelectionModel().clearSelection();
        fromPostCombo.getSelectionModel().clearSelection();
        toCityCombo.getSelectionModel().clearSelection();
        toPostCombo.getSelectionModel().clearSelection();
    }
    
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }
    
    private boolean checkInputs() {
        if (classCombo.getValue() == null) {
            statusLabel.setText("Valitse luokka!");
            return false;
        }
        else if (fromPostCombo.getValue() == null)
                return false;
        else if (toPostCombo.getValue() == null)
                return false;
        else return true;
    }
    
    private boolean checkInputFields() {
        if (itemNameField.getText() == null)
            return false;
        else if (itemSizeField.getText() == null)
            return false;
        else if (itemWeightField.getText() == null)
            return false;
        else if (isInteger(itemSizeField.getText()) == false)
            return false;
        else if (isInteger(itemWeightField.getText()) == false)
            return false;
        else return true;
    }
}
