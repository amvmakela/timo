/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author am
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Button updatePackageButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button clearMapButton;
    @FXML
    private Button mapButton;
    @FXML
    private WebView web;
    @FXML
    private ComboBox<Package> packageCombo;
    @FXML
    private Button sendButton;
    @FXML
    private ComboBox<String> cityCombo;
      
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        /* Filling the combobox with cities */
        DataBuilder db = DataBuilder.getInstance();
        
        SmartPosts sp = SmartPosts.getInstance();
        int size = sp.getListSize();
        
        for (int i = 0; i < size; i++) {
            String c = sp.getSmartPost(i).getCity();
            if (cityCombo.getItems().contains(c) != true) {
                cityCombo.getItems().add(c);
            }
        }
    }    


    @FXML
    private void createPackage(ActionEvent event) {
        
        try {
            Stage packageWindow = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageCreation.fxml"));
            
            Scene scene = new Scene(page);
            
            packageWindow.setScene(scene);
            packageWindow.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void clearMap(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void addToMap(ActionEvent event) {
        
        SmartPosts sp = SmartPosts.getInstance();
        int size = sp.getListSize();
        String city = cityCombo.getValue();
        
        /* Finding all the smartposts for a specific city */
        for (int i = 0; i < size; i++) {
            SmartPost s = sp.getSmartPost(i);
            if (s.getCity().equals(city)){

                String js = "document.goToLocation(" + "'" + s.getAddress() + "'" + ", "
                            + "'" + s.getInfo() + "'" + ", 'blue'" + ")";

                web.getEngine().executeScript(js);
            }
        }
    }

    @FXML
    private void updatePackageList(ActionEvent event) {
        packageCombo.getItems().clear();
        Storage s = Storage.getInstance();
        int size = s.getSize();
        for (int i = 0; i < size; i++) {
            packageCombo.getItems().add(s.getPackage(i));
        }
    }

    @FXML
    private void sendAction(ActionEvent event) {
        Package p = packageCombo.getValue();
        String addressArray = p.getPackageAddresses();
        int c = p.getPackageClass();
        String js = "document.createPath(" + addressArray + ", " + "'red', " + c + ")";
        web.getEngine().executeScript(js);
        
        /* Removing the sent package from the combobox and the storage*/
        Storage s = Storage.getInstance();
        s.removePackage(p);
        packageCombo.getItems().remove(p);
        
    }
    
}
