/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author am
 */
class Object {
    private String name;
    private int size;
    private int weight;
    
    public Object(String n, int s, int w) {
        name = n;
        size = s;
        weight = w;
    }
    
    public int getSize() {
        return size;
    }
    
    public int getWeight() {
        return weight;
    }
    
    @Override
    public String toString() {
        return (name);
    }
}

class Flowerpot extends Object {
    private boolean fragile = true;
    public Flowerpot(String n, int s, int w) {
        super(n, s, w);
    }
}

class Spoon extends Object {
    private boolean shiny = true;
    public Spoon(String n, int s, int w) {
        super(n, s, w);
    }
}

class Bicycle extends Object {
    private String color;
    public Bicycle(String n, int s, int w, String c) {
        super(n, s, w);
        this.color = c;
    }
}

class Rock extends Object {
    private int density;
    public Rock(String n, int s, int w, int t) {
        super(n, s, w);
        this.density =  t;
    }
}
